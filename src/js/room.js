"use strict";

export class Room {
  constructor(x, y, ground = [], randomEvent) {
    this.x = x;
    this.y = y;
    this.ground = ground;
    this.randomEvent = randomEvent;
  }

  trader() {
    let i = ['armory', 'sword master', 'rogue master', 'thaumaturge'];
    return i[Math.floor(Math.random() * i.length)]
  }

  eventGeneration() {
    let tab = ['empty', 'ennemy', 'enygma', 'treasure', 'ennemy', 'trap', 'trapped-treasure', 'ennemy', 'boss', 'trader'];
    let random = tab[Math.floor(Math.random() * tab.length)];
    if (random === 'trader') {
      let trade = this.trader()
      if (trade === 'armory') {
        random = 'armory';
      } else if (trade === 'sword master') {
        random = 'sword master';
      } else if (trade === 'rogue master') {
        random = 'rogue master';
      } else if (trade === 'thaumaturge') {
        random = 'thaumaturge';
      }
    }
    return random;
  }
}

