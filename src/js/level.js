import { Room } from './room'

"use strict";

export class Level {
  constructor(level) {
    this.level = level;
  }
  generate() {
    let grid = [];
    for (let x = 0; x < this.level; x++) {
      grid[x] = [];
      for (let y = 0; y < this.level; y++) {
        let room = grid[x][y];
        room = new Room(x, y);
        room.randomEvent = room.eventGeneration();
        grid[x].push(room);
      };
    }
    return grid;
  }
}